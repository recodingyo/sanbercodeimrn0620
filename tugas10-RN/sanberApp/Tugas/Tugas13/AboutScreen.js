import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { FontAwesome } from "@expo/vector-icons";
import Icon from "react-native-vector-icons/MaterialIcons";

export default class App extends React.Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Text style={{ fontSize: 36, fontWeight: "bold", color: "#003366" }}>
            Tentang Saya
          </Text>
          <Icon name="account-circle" size={200} style={{ color: "#DFDFDF" }} />
          <Text style={{ fontSize: 24, fontWeight: "bold", color: "#003366" }}>
            DeeAitch
          </Text>
          <Text style={{ fontSize: 16, fontWeight: "bold", color: "#3EC6FF" }}>
            React Native Developer
          </Text>
          <View style={styles.portfolio}>
            <Text style={{ fontSize: 18, color: "#003366" }}>Portfolio</Text>
            <View
              style={{
                borderBottomColor: "#003366",
                borderBottomWidth: 1,
                color: "#003366",
              }}
            />
            <View style={styles.gitAccount}>
              <TouchableOpacity style={styles.gitItem}>
                <FontAwesome name="gitlab" size={40} color="#3EC6FF" />
                <Text style={styles.gitUser}>@deeaitch</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.gitItem}>
                <FontAwesome name="github" size={40} color="#3EC6FF" />
                <Text style={styles.gitUser}>@deeaitch</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.contact}>
            <Text style={{ fontSize: 18, color: "#003366" }}>Hubungi Saya</Text>
            <View
              style={{
                borderBottomColor: "#003366",
                borderBottomWidth: 1,
              }}
            />
            <View style={styles.contactAccount}>
              <TouchableOpacity style={styles.contactItem}>
                <FontAwesome name="facebook-square" size={40} color="#3EC6FF" />
                <Text style={styles.contactUser}>@deeaitch</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.contactItem}>
                <FontAwesome name="instagram" size={40} color="#3EC6FF" />
                <Text style={styles.contactUser}>@deeaitch</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.contactItem}>
                <FontAwesome name="twitter" size={40} color="#3EC6FF" />
                <Text style={styles.contactUser}>@deeaitch</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 50,
  },
  profile: {
    color: "grey",
  },
  portfolio: {
    marginTop: 15,
    padding: 5,
    height: 140,
    width: 300,
    backgroundColor: "#EFEFEF",
  },
  gitAccount: {
    flexDirection: "row",
    justifyContent: "space-around",
    paddingVertical: 20,
  },
  gitItem: {
    alignItems: "center",
    justifyContent: "center",
  },
  gitUser: {
    fontSize: 12,
    fontWeight: "bold",
    paddingTop: 10,
    color: "#003366",
  },
  contact: {
    marginTop: 15,
    padding: 5,
    height: 250,
    width: 300,
    backgroundColor: "#EFEFEF",
  },
  contactAccount: {
    justifyContent: "space-evenly",
    paddingVertical: 20,
  },
  contactItem: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    marginBottom: 20,
  },
  contactUser: {
    fontSize: 12,
    fontWeight: "bold",
    paddingLeft: 20,
    color: "#003366",
  },
});
