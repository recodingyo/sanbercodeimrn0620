import React from "react";
import { StyleSheet, View, Text } from 'react-native';

export default () => (
    <View style={styles.container}>
        <Text>Halaman Proyek</Text>
    </View>
)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: "center",
    }
})