import React from "react";
import { StyleSheet, Text, View, Image, FlatList } from "react-native";
import { FontAwesome } from "@expo/vector-icons";
import SkillCard from "./SkillCard";
import skillData from "./skillData.json";

export default class SkillPage extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoBox}>
          <Image
            source={require('../Tugas13/images/logo.png')}
            style={{ width: 187, height: 51 }}
          />
        </View>

        <View style={styles.userBox}>
          <FontAwesome
            style={{ paddingTop: 5 }}
            name="user-circle"
            size={26}
            color="#3EC6FF"
          />
          <View style={styles.userText}>
            <Text style={styles.hai}>Hai,</Text>
            <Text style={styles.username}>Tri Sugihartono</Text>
          </View>
        </View>
        <Text style={styles.skillText}>SKILL</Text>

        <View style={styles.line} />

        <View style={styles.categoryBox}>
          <View style={styles.categoryDetail}>
            <Text style={styles.fontskilldetail}>Library/Framework</Text>
          </View>
          <View style={styles.categoryDetail}>
            <Text style={styles.fontskilldetail}>Bahasa Pemrograman</Text>
          </View>
          <View style={styles.categoryDetail}>
            <Text style={styles.fontskilldetail}>Teknologi</Text>
          </View>
        </View>
        <View style={styles.listSkill}>


          <FlatList
            data={skillData.items}
            renderItem={(skill) => <SkillCard skill={skill.item} />}
            keyExtractor={(item) => item.id}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  logoBox: {
    backgroundColor: "white",
    paddingHorizontal: 15,
    alignItems: "flex-end",
  },
  userBox: {
    backgroundColor: "white",
    paddingHorizontal: 15,
    flexDirection: "row",
  },
  userText: {
    alignContent: "space-around",
    paddingLeft: 10,
  },
  hai: {
    color: "#000000",
    fontSize: 12,
  },
  username: {
    color: "#003366",
    fontSize: 16,
  },
  skillText: {
    color: "#003366",
    fontSize: 36,
    paddingLeft: 15,
    marginTop: 20,
  },
  line: {
    height: 4,
    backgroundColor: "#3EC6FF",
    width: 343,
    alignSelf: "center",
  },
  categoryBox: {
    flexDirection: "row",
    justifyContent: "space-around",
    paddingTop: 10,
  },
  categoryDetail: {
    backgroundColor: "#B4E9FF",
    justifyContent: "center",
    borderRadius: 8,
    padding: 7,
  },
  fontskilldetail: {
    color: "#003366",
    fontSize: 12,
    fontWeight: "bold",
  },
  listSkill: {
    flexDirection: "column",
    paddingTop: 10,
  },
});

