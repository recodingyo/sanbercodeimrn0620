import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

export default class SkillItem extends React.Component {
  render() {
    let skill = this.props.skill;
    return (
      <View style={styles.listContainer}>
        <View style={styles.logoContainer}>
          <MaterialCommunityIcons
            name={skill.iconName}
            size={70}
            color="#003366"
          />
        </View>

        <View style={styles.textContainer}>
          <Text style={styles.skillName}>{skill.skillName}</Text>
          <Text style={styles.category}>{skill.categoryName}</Text>
          <View style={{ alignItems: "flex-end" }}>
            <Text style={styles.percent}>{skill.percentageProgress}</Text>
          </View>
        </View>
        <TouchableOpacity style={styles.logoContainer}>
          <MaterialCommunityIcons
            name="chevron-right"
            color={"#003366"}
            size={80}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    marginHorizontal: 20,
    padding: 7,
    borderRadius: 8,
    backgroundColor: "#B4E9FF",
    shadowColor: "#000",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 8,
  },
  logoContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
  textContainer: {
    flex: 1,
    paddingHorizontal: 20,
  },
  skillName: {
    fontSize: 24,
    lineHeight: 24,
    fontWeight: "600",
    color: "#003366",
  },
  category: {
    fontSize: 14,
    lineHeight: 15,
    fontWeight: "400",
    color: "#3EC6FF",
  },
  percent: {
    fontSize: 48,
    fontWeight: "600",
    lineHeight: 53,
    color: "white",
  },
});

