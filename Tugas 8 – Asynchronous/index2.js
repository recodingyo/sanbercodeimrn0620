// Masih di folder yang sama dengan promise.js, buatlah sebuah file dengan nama index2.js. 
//Tuliskan code sebagai berikut

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
var time=10000
var i=0;
function letsread(time,books){
    if(time>0 && i<books.length)
    readBooksPromise(time,books[i])
        .then(function (sisaWaktu) {
            time=sisaWaktu;
            i++;
            letsread(time,books);
        })
        .catch(function (sisaWaktu) {
            return;
        });
}

letsread(time,books)

// Lanjutkan code untuk menjalankan function readBooksPromise 

// Lakukan hal yang sama dengan soal no.1, habiskan waktu selama 10000 ms (10 detik) 
//untuk membaca semua buku dalam array books.!
