function arrayToObject(arr) {
    const thisYear = new Date().getFullYear(); //untuk mengambil tanggal saat ini
    var obj = {}; //variabel obj untuk menampung hasil
    arr.forEach(el => {
        var insideObj = {}; //data objek yang akan ditampilkan
        insideObj.firstName = el[0];
        insideObj.lastName = el[1];
        insideObj.gender = el[2];
        if (el[3] > thisYear || !el[3]) insideObj.age = "Invalid birth year"; //kondisi umur dari person  // jika umurnya lebih besar dari value tahun saat ini atau kosong, maka akan menampilkan invalid
        else insideObj.age = el[3];
        obj[el[0] + ' ' + el[1]] = insideObj; //menggabungkan firstname dengan lastname
    });
    console.log(obj);

}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""


//No 2 shopping time
function shoppingTime(memberId, money) {
    var listItems = {   //list Item di Toko dan untuk shopping
        'Sepatu Stacattu': 1500000,
        'Baju Zoro': 500000,
        'Baju H&N' : 250000,
        'Sweater Uniklooh': 175000,
        'Casing Handphone' : 50000
    };
    var listPurchased = [];
    var total = 0;
    var currentMoney = money;
    var lowerLimit = 50000;

    if (!memberId || memberId === '') { //kondisi jika Member ID kosong
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    } else if (money < lowerLimit) {  //kondisi unag yang dimiliki lebih kecil dari harga barang nya
        return 'Mohon maaf, uang tidak cukup';
    }

    Object.entries(listItems).map(([key, val]) => {
        if (currentMoney >= val) {
            listPurchased.push(key);
            currentMoney -= val;
            total += val;
        }
    })
    return {
        memberId: memberId,
        money: money,
        listPurchased: listPurchased,
        changeMoney: money - total
    };

  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja



  //Soal No 3 Naik Angkot
  function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];  //rute angkot
    var pricePerRoute = 20000; //harga per route
    var result = [];

    arrPenumpang.forEach(el => {
        var name = el[0];
        var start = el[1];
        var finish = el[2];
        var distance = Math.abs(rute.indexOf(start) - rute.indexOf(finish)); //menghitung jarak rute
        var price = distance * pricePerRoute; //menghitung harga ongkos angkutan
        result.push({ 
            'penumpang': name,
            'naikDari': start,
            'tujuan': finish,
            'bayar': price
        });
    });

    return result;

  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]
