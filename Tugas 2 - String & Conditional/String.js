//Soal String
//Soal No. 1 (Membuat kalimat)

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var StringGabung = word.concat(second).concat(third).concat(fourth).concat(fifth).concat(sixth).concat(seventh)
var StringGabung2 =word+" "+second+" "+third+" "+fourth+" "+fifth+" "+sixth+" "+seventh
console.log('\n\n============ Output Soal No 1 Buat Kalimat ====================\n\n'+StringGabung2)

//Soal No.2 Mengurai kalimat (Akses karakter dalam string)
var sentence = "I am going to be React Native Developer"; 
var FirstWord = sentence[0] ; 
var SecondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] +sentence[9] ; // lakukan sendiri 
var fourthWord = sentence[11] + sentence[12] ; // lakukan sendiri 
var fiftWord = sentence[14] + sentence[15] ; // lakukan sendiri 
var sixthWord =sentence[17] + sentence[18] + sentence[19] + sentence[20] +sentence[21] ; // lakukan sendiri 
var seventhWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] +sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38] ; // lakukan sendiri 
console.log('\n\n============ Output Soal No.2 Mengurai kalimat (Akses karakter dalam string) ====================\n\n'+'First Word: ' + FirstWord+'\n' +'Second Word: ' + SecondWord +'\n' +'Third Word: ' + thirdWord+'\n' +'Fourth Word: ' + fourthWord+'\n' +'Fifth Word: ' + fiftWord+'\n' +'Sixth  Word: ' + sixthWord+'\n' +'Seventh  Word: ' + seventhWord); 
//
//
//
//
//Soal No. 3 Mengurai Kalimat (Substring)
var sentence2 = 'wow JavaScript is so cool'; 
var firstWord2 = sentence2.substring(0,3); 
var secondWord2 = sentence2.substring(4,14); // do your own! 
var thirdWord2 = sentence2.substring(15,17); // do your own! 
var fourthWord2 = sentence2.substring(18,20); // do your own! 
var fifthWord2 = sentence2.substring(21,25); // do your own! 
console.log('\n\n============ Output Soal No. 3 Mengurai Kalimat (Substring) ====================\n\n'+'First Word: ' + firstWord2+'\nSecond Word: ' + secondWord2 +'\nThird Word: ' + thirdWord2+'\nFourth Word: ' + fourthWord2+'\nFifth Word: ' + fifthWord2); 
//
//
//
//
//Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String
var sentence3 = 'wow JavaScript is so cool'; 
var firstWord3 = sentence3.substring(0, 3); 
var secondWord3 =  sentence3.substring(4,14); // do your own! 
var thirdWord3 =  sentence3.substring(15,17); // do your own! 
var fourthWord3 =  sentence3.substring(18,20); // do your own! 
var fifthWord3 =  sentence3.substring(21,25); // do your own! 

var firstWordLength = firstWord3.length ;
var secondWordLength = secondWord3.length ;
var thirdWordLength = thirdWord3.length ;
var fourthWordLength = fourthWord3.length ;
var fifthWordLength = fifthWord3.length  ;

console.log('\n\n============ Output Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String ====================\n\n'+'First Word: ' + firstWord3 + ', with length: ' + firstWordLength+'\nSecond Word: ' + secondWord3 + ', with length: ' + secondWordLength+'\nThird Word: ' + thirdWord3 + ', with length: ' + thirdWordLength+'\nFourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength+'\nFifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 
