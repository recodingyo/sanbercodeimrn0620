//Soal Conditional
// Soal 1 If Else
var nama = "Jhon"
var peran = ""
if (nama == "Jhon" && peran =="") {
    console.log("\n============ Output If Else  ==============\nHalo John, Pilih peranmu untuk memulai game!")
} else if (nama == "Jane" && peran == "Penyihir" ) {
    console.log("\n============ Output If Else  ==============\nSelamat datang di Dunia Werewolf, Jane \nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")    
} else if( nama == "Jenita" && peran == "Guard") {
    console.log("\n============ Output If Else  ==============\nSelamat datang di Dunia Werewolf, Jenita \nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (  nama == "Junaedi" && peran == "Werewolf" ) {
    console.log("\n============ Output If Else  ==============\nSelamat datang di Dunia Werewolf, Junaedi \nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")   
} else  {
    console.log("\n============ Output If Else  ==============\nNama Harus diisi!")
}


// Soal 2 Switch Case
var hari = 21;
var bulan = 1;
var tahun = 1945;

switch(bulan) {
 case 0: bulan = "Januari"; break;
 case 1: bulan = "Februari"; break;
 case 2: bulan = "Maret"; break;
 case 3: bulan = "April"; break;
 case 4: bulan = "Mei"; break;
 case 5: bulan = "Juni"; break;
 case 6: bulan = "Juli"; break;
 case 7: bulan = "Agustus"; break;
 case 8: bulan = "September"; break;
 case 9: bulan = "Oktober"; break;
 case 10: bulan = "November"; break;
 case 11: bulan = "Desember"; break;
}
var tampilTanggal = "\n============ Output Switch Case ==============\nTanggal: " + hari + " "+ bulan + " " + tahun +"\n\n";
console.log(tampilTanggal);