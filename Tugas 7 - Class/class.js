// 1. Animal Class 
// Terdapat sebuah class Animal yang memiliki sebuah constructor name, 
//default property legs = 4 dan cold_blooded = false.

// Release 0
// Buatlah class Animal yang menerima satu parameter constructor berupa name. 
//Secara default class Animal akan memiliki property yaitu legs (jumlah kaki) yang bernilai 4 dan 
//cold_blooded bernilai false.

// Gunakan method getter untuk mengakses property di dalam class
console.log("===No1. Animal Class realease 0===")
class Animal {
  constructor(name,legs=4, cold_blooded='false'){
		this.nama=name;
		this.kaki=legs;
		this.darah_dingin=cold_blooded;
	}
	name(){
		return this.nama;
	}
	legs(){
		return this.kaki;
	}
	cold_blooded() {
    return false;
  }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name()) // "shaun"
console.log(sheep.legs()) // 4
console.log(sheep.cold_blooded()) // false


// Release 1
// Buatlah class Frog dan class Ape yang merupakan inheritance dari class Animal. 
//Perhatikan bahwa Ape (Kera) merupakan hewan berkaki 2, hingga dia tidak menurunkan sifat 
//jumlah kaki 4 dari class Animal. class Ape memiliki function yell() yang menampilkan “Auooo” 
//dan class Frog memiliki function jump() yang akan menampilkan “hop hop”.
console.log("===No.1 Release 1===")
class Ape extends Animal {
  yell() {
    console.log('Auooo');
  }
}
class Frog extends Animal {
  jump() {
    console.log('hop hop');
  }
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

// 2. Function to Class
// Terdapat sebuah class dengan nama Clock yang ditulis seperti penulisan pada function, 
//ubahlah fungsi tersebut menjadi class dan pastikan fungsi tersebut tetap berjalan dengan baik. 
//Jalankan fungsi di terminal/console Anda untuk melihat hasilnya. (tekan tombol Ctrl + C pada terminal /
//untuk menghentikan method clock.start())

// Hint: Fokus soal ini hanya pada kegiatan mengubah struktur function Clock menjadi class. 
//Jangan lupa menambahkan constructor di dalam class, dan ubah function di dalam Clock menjadi 
//method pada class.
console.log("===No.2. Function to Class===")
class Clock {
	constructor({template}) {
		this.template = template;
	}
	render() {
		var date = new Date();

		let hours = date.getHours();
		if (hours < 10) hours = '0' + hours;

		let mins = date.getMinutes();
		if (mins < 10) mins = '0' + mins;

		let secs = date.getSeconds();
		if (secs < 10) secs = '0' + secs;
		let output = this.template
		.replace('h', hours)
		.replace('m', mins)
		.replace('s', secs);

		console.log(output);
	}
	stop() {
		clearInterval(this.timer);
	}
	start() {
		this.render();
		this.timer = setInterval(() => this.render(), 1000);
	}
}
var clock = new Clock({template: 'h:m:s'});
clock.start(); 
