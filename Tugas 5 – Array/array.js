// //no1 - Range
// Buatlah sebuah function dengan nama range() yang menerima dua parameter berupa number. 
//Function mengembalikan sebuah array yang berisi angka-angka mulai dari angka parameter pertama 
//hingga angka pada parameter kedua. Jika parameter pertama lebih besar dibandingkan parameter kedua 
//maka angka-angka tersusun secara menurun (descending).

// struktur fungsinya seperti berikut range(startNum, finishNum) {}
// Jika parameter pertama dan kedua tidak diisi maka function akan menghasilkan nilai -1
function range(startNum, finishNum) {
    var numbers = []
    if(startNum < finishNum){ //angka kedua > angka pertama 
        for(x = startNum; x <= finishNum; x++){ // angka akan bertambah dan akan menyususn secara ascending
            numbers.push(x); //untuk menambahkan nilai di element terakhir
        }
    }else if(startNum > finishNum){ //angka pertama > angka kedua
      for(x = startNum; x >= finishNum; x--){ // angka akan berkuang dan menyusun secara descending
            numbers.push(x); // untuk menambahkan nilai 
        }
    }else{
        numbers = -1
    }
  
    return numbers
  }
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


//No2 - Range with Step
//Jika parameter pertama lebih besar dibandingkan parameter kedua 
//maka angka-angka tersusun secara menurun (descending) dengan step sebesar parameter ketiga.
function rangeWithStep(startNum, finishNum,step) {
    var numbers = []
    if(startNum < finishNum){  //kkondisi true--angka kedua lebih besar
        for(x = startNum; x <= finishNum; x+=step){ //angka akan bertambah (ascending) dengan step 
            numbers.push(x);
        }
    }else if(startNum > finishNum){ //kondisi false
      for(x = startNum; x >= finishNum; x-=step){
            numbers.push(x);
        }
    }else{
        numbers = -1
    }
  
    return numbers
  }
  console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
  console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
  console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
  console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

  //no3
function sum(startNum, finishNum, step=1){
	if(!startNum){
		return 0
	}else if(!finishNum){
		return 1
	}else{
		numbers = rangeWithStep(startNum, finishNum, step)
		return numbers.reduce((a,b) => a + b,0)
	}

}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

  //No 4
  function dataHandling() {
    var jawaban = '';
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ]
    for (var i = 0; i < input.length; i++) {
        jawaban += "Nomor Id: " + input[i][0] + "\n"
        jawaban += "Nama Lengkap: " + input[i][1] + "\n"
        jawaban += "TTL: " + input[i][2] + "\n"
        jawaban += "Hobi: " + input[i][3] + "\n\n"
    }
    return jawaban
}
 
console.log(dataHandling())
//no5
function balikKata(kata){
	var splitKata = kata.split('');
	var reverseKata = splitKata.reverse();
	var joinKata = reverseKata.join('');

	return joinKata;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

  //n06

  function dataHandling2(input) {
    var jawaban = input;
    jawaban.splice(1, 1, "Roman Alamsyah Elsharawy");
    jawaban.splice(2, 1, "Provinsi Bandar Lampung");
    jawaban.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(jawaban);
 
    var ttl = jawaban[3]
    var moon_array = ttl.split("/")
    var moon_array2 = ttl.split("/")
    var moon = moon_array[1];
    var nama = jawaban[1];
    switch(parseInt(moon)) {
        case 01 : {moon = "January"; break;}
        case 02 : {moon = "February"; break;}
        case 03 : {moon = "Maret"; break;}
        case 04 : {moon = "April"; break;}
        case 05 : {moon = "Mei"; break;}
        case 06 : {moon = "Juni"; break;}
        case 07 : {moon = "Juli"; break;}
        case 08 : {moon = "Agustus"; break;}
        case 09 : {moon = "September"; break;}
        case 10 : {moon = "Oktober"; break;}
        case 11 : {moon = "November"; break;}
        case 12 : {moon = "Desember"; break;}
        default : {moon = "moon tidak valid"}
    }
    console.log(moon);
    // sort
    moon_array.sort((a,b)=>b-a) ;
    console.log(moon_array);
    // 21-05-1989
    console.log(moon_array2.join("-"))
    // nama dibatas 15
    console.log(nama.slice(0,15));
}
 
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);