//Tugas HArian --Looping 
//No. 1 Looping While
console.log("LOOPING PERTAMA");
var angka = 2;
while(angka <= 20) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(angka + ' - I love coding'); // Menampilkan nilai flag pada iterasi tertentu
  angka = angka+2; // Mengubah nilai flag dengan menambahkan 1
}

console.log("\nLOOPING KEDUA");
var back = 20;
while(back >= 1) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(back + ' - I will become a mobile developer'); // Menampilkan nilai flag pada iterasi tertentu
  back = back-2; // Mengubah nilai flag dengan menambahkan 1
}


//No 2. Looping menggunakan For
console.log("\nOUTPUT");
for(var angka = 1; angka <= 20; angka++) {
  if(angka % 2 == 1){
    if(angka % 3 == 0){
      console.log(angka + ' - I Love Coding');
    }else{
      console.log(angka + ' - Santai');
    }
  }else{
    console.log(angka + ' - Berkualitas');
  }
} 


//No. 3 Membuat Persegi Panjang 8x4
var himpun = '';
for(var baris = 1; baris <= 4; baris++){
  for(var kolom = 1; kolom <= 8; kolom++){
    himpun += '#';
  }
  himpun += '\n';
}
console.log('\n'+himpun);


//No. 4 Membuat Tangga
var himpun = '';
for(var baris = 1; baris <= 7; baris++){
  for(var kolom = 1; kolom <= baris; kolom++){
    himpun += '#';
  }
  himpun += '\n';
}
console.log('\n'+himpun);


//No. 5 Membuat Papan Catur
var himpun = '';
for(var baris = 1; baris <= 8; baris++){
  for(var kolom = 1; kolom <= 8; kolom++){
    if(baris % 2 == 0){
      if(kolom % 2 == 0){
        himpun += ' ';
      }else{
        himpun += '#';
      }
    }else{
      if(kolom % 2 == 0){
        himpun += '#';
      }else{
        himpun += ' ';
      }
    }
  }
  himpun += '\n';
}
console.log('\n' +himpun);



